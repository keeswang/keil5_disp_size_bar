
# Keil5_disp_size_bar V0.4


## 简介


[Keil5_disp_size_bar]
以进度条百分比来显示keil编译后代码对芯片的内存ram和存储flash的占用情况。
![img_disp_size_bar](https://img.anfulai.cn/dz/attachment/forum/202307/07/094212tlip6kpfsqf2ska1.png)
<br>

原理是使用C语言遍历目录找到keil工程生成出的.map文件，然后找到对应的ram和flash的总大和占用大小，
然后以进度条和百分比的直观格式输出来，以便嵌入式软件工程师方便调试，知道芯片占用情况，进行裁剪和优化。
基本能找到编译后生成的map文件就能输出生成的代码对ram和flash的占用大小百分比进度条，只要能找到.map文件，keil5环境下通用。

## 版本更新日志
. 更新到v0.4
- 把uint64_t改为uint32_t,因为发现sscanf函数的%x给赋值时，64位好像会因为对齐问题错误。
- 修复递归查找不彻底导致子目录下的map找不到
  
- 添加C51支持8051 测试STC89C52和WCH552均测试可以，但是部分工程没有写xram的大小的默认只能以iram大小替代。
还有部分工程是没有在工程定义真正大小的，也就是芯片本身没有keil开发包，用其他芯片或通用开发包定义的工程。
在keil上是没有定义对应芯片型号或用了其他芯片的定义的8051这就会导致显示占用的最大值错误。
要确保工程文件<CPU>里面的IRAM,XRAM,IROM都是正确的才行。

- 同时发现部分例如stm32F0系列的工程map文件格式不同，可能早期map不怎么统一规范，给的max都是0xFFFFFFFF,无法输出进度条，只能读工程的里芯片定义的max，来替代map文件的max，所以部分自定义的显示不出来，工程文件有些格式也是会不一样有的以逗号分隔有的以-分隔。

. 更新到v0.3
- 修改进度条部分字符对齐问题,方块字符选择了正方形等宽
- 根据网友的反馈由于关键词(Exec关键词有部分map文件不存在，关键词改成Execution Region检索执行段
- 加大ram和flash的占用信息存储数组的上限为30，有的map的flash分开的段比较多，或者自定义了.bss的内存池
- 根据网友反馈使用了自定义malloc的内存池的ram被归类为了flash，除了带RAM外添加带ER$$的也视为RAM
- 百分比修改以KB单位的显示占用分子分母
- 最后以B为单位显示剩余可用空间
- %d显示改为%u显示无符号整数
  
. 更新到V0.2
- 更改进度条样式
- 采用关键词(Exec模糊检索ram和flash的size和max
- 支持多个ram和flash的占用百分比进度条显示

## 使用方法：

1. 把程序Keil5_disp_size_bar.exe放到工程目录下,<br>
要放在在.map文件更上一层的目录,<br>
例如可以放在和工程文件.uvoptx同一目录下。<br>

![img_dir](https://img.anfulai.cn/dz/attachment/forum/202307/05/120244vxg770io5p70i2f7.png)
<br>

2. 在工程打开魔术棒配置，在User的After Build/Rebuild下<br>
添加编译后执行程序#Run1或#Run2,<br>
在前面打钩，后面则选择要执行的程序Keil5_disp_size_bar.exe对应路径<br>
注意要选对是当前工程目录下的Keil5_disp_size_bar.exe<br>
如果选错其他工程的，编译出的信息则是其他工程的map文件<br>

![img_after_bild](https://img.anfulai.cn/dz/attachment/forum/202307/05/120258i24obuo9oue4e9bz.png)
<br>

3. 接着每次对工程按下编译，编译完成后就能看到生成的代码对ram和flash的占用大小百分比进度条。<br>
![img_disp_size_bar](https://img.anfulai.cn/dz/attachment/forum/202307/06/085550y1aaanv2mejif4vh.png)
<br>

4. 如果执行了Keil5_disp_size_bar.exe，却没有输出占用百分比进度条，<br>
请检测输出的错误消息,最大可能是当前工程或者你放置程序的目录下递归查找也找不到map文件，<br>
请检测工程的Output输出生成配置，然后按下全部重新编译一次再看看能不能输出占用百分比进度条。<br>

![img_find_map](https://img.anfulai.cn/dz/attachment/forum/202307/05/120332axbkjl1jdel7vjse.png)


-  一个极客 ageek nikola 开源



